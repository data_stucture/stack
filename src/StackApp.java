public class StackApp {
    public static void main(String[] args) {
        StackX ts = new StackX(200);
        int num = 1;
        while (!ts.isFull()) ts.push(6516000 + num++);
        while (!ts.isEmpty()) System.out.println(ts.pop());

        StackXChar tsc = new StackXChar(10);
        String str = "kaO tsixeS";
        int pointer = 0;
        
        while(!tsc.isFull()) tsc.push(str.charAt(pointer++));
        while(!tsc.isEmpty()) System.out.print(tsc.pop());
        System.out.println();

        Reverser rev = new Reverser(str);
        System.out.println("Original: " + str);
        System.out.println("Reversed: " + rev.doRev());

    }
}
