public class StackXChar {
    private int MaxSize;
    private char[] stackArray;
    private int top = -1;

    public StackXChar(int s) {
        MaxSize = s;
        stackArray = new char[MaxSize];
    }

    public int getLength() {
        return MaxSize;
    }

    public void push(char j) {
        stackArray[++top] = j;
    }

    public char pop() {
        return stackArray[top--];
    }

    public char peek() {
        return stackArray[top];
    }

    public boolean isEmpty() {
        return (top == -1);
    }

    public boolean isFull() {
        return (top == MaxSize-1);
    }
}
